# Custom ATT Pre-Popup

## Integration Steps

1) Import the **iOS 14 Advertising Support** package from the Package Manager in Unity.

2) **"Install"** or **"Upload"** FG Custom PrePopup plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button from FunGames Integration window to fill up your scene with required components and create the Settings asset.

## Remote Config Setup

In order to A/B test different display for the ATT Prepopup, you can use the "CustomPrePopupDisplay" remote config variable and set is as follow:

<p align="center">
<figure>
    <img src="_source/customPrepopup0.png" width="160" height="320" style="margin: 20px"/>
    <figcaption>CustomPrePopupDisplay=0</figcaption>
</figure>
<figure>
    <img src="_source/customPrepopup1.png" width="160" height="320" style="margin: 20px"/>
    <figcaption>CustomPrePopupDisplay=1</figcaption>
</figure>
<figure>
    <img src="_source/customPrepopup2.png" width="160" height="320" style="margin: 20px"/>
    <figcaption>CustomPrePopupDisplay=2</figcaption>
</figure>
</p>